#!/usr/bin/env python3
import vk_api
import networkx as nx
import argparse
import re
from tqdm import tqdm
from os.path import join

parser = argparse.ArgumentParser('Friends graph')

parser.add_argument('login', type=str, help='Your vk login')
parser.add_argument('password', type=str, help='Your vk password')
parser.add_argument('target', type=str,
                    help='Target id or screen name (idXXXXXX, XXXXXX, sCrEeN0_0NaMe)')
parser.add_argument('--out-path', '-o', metavar='PATH', dest='out', default='./')

def get_id(arg):
    match_num = re.match(r'(?:id)?(\d+)', arg)
    match_screen = re.match(r'(\w+)', arg)
    if match_num is not None:
        return int(match_num.group(1))
    elif match_screen is not None:
        return vk.users.get(user_ids=match_screen.group(1))[0]['id']
    else:
        raise ValueError(f'Invalid user id or screen name: {arg}')


def filter_friends(friends):
    return list(map(lambda fr: {'id': fr['id'], 'first_name': fr['first_name'], 'last_name': fr['last_name']},
                    filter(lambda fri: 'deactivated' not in fri and not fri['is_closed'], friends)))


def get_friends(id):
    params = {'fields': 'sex'}
    fp = vk.friends.get(user_id=id, **params)
    if fp['count'] == len(fp['items']):
        return filter_friends(fp['items'])
    result = []
    for offset in range(0, fp['count'], 5000):
        result += vk.friends.get(user_id=id, offset=offset, **params)
    return filter_friends(result)



def main(args):
    print('Checking ID')

    vk_session = vk_api.VkApi(args.login, args.password,
                              auth_handler=lambda: (input('Two factor code: '), True))  # app_id=2685278
    vk_session.auth()
    global vk
    vk = vk_session.get_api()

    target_id = get_id(args.target)
    user_info = vk.users.get(user_ids=target_id)[0]

    print(
        f'Selected ID: {target_id}, user: {user_info["first_name"]} {user_info["last_name"]} (https://vk.com/id{target_id})')
    if user_info['is_closed']:
        raise ValueError('Account is closed')

    print('Getting open friends')
    friends = get_friends(target_id)

    print(f'User has {len(friends)} open friends')
    friends_friends = {}

    print('Getting friends`s open friends')
    for friend in tqdm(friends, desc='Getting friends'):
        friends_friends[friend['id']] = get_friends(friend['id'])

    friends_graph = nx.Graph()
    friends_graph.add_edges_from(
        map(lambda fr: (target_id, fr['id']), friends))
    for friend, his_friends in friends_friends.items():
        friends_graph.add_edges_from(
            map(lambda fr: (friend, fr['id']), his_friends))

    print(
        f'Total {friends_graph.number_of_nodes()} nodes and {friends_graph.number_of_edges()} edges')

    print('Saving edgelist')
    nx.to_pandas_edgelist(friends_graph).to_csv(join(args.out, f'{target_id}.csv'), index=False)
    print('Saving gexf')
    nx.write_gexf(friends_graph, join(args.out, f'{target_id}.gexf'))


if __name__ == "__main__":
    main(parser.parse_args())

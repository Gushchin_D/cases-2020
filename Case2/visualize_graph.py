#!/usr/bin/env python3
import argparse
from time import time
import pandas as pd
import networkx as nx
import plotly.graph_objects as go
import plotly.io as pio

pio.templates.default = 'plotly_dark'

parser = argparse.ArgumentParser('Ployly graph visualizer')
parser.add_argument('file', help='File to visualize (edge list in .csv)')
parser.add_argument('--layout', '-l', metavar='LAYOUT', help='Layout for visualing graph in plotly',
                    choices=['kamada_kawai', 'fruchterman_reingold'], default='fruchterman_reingold')


def display_with_plotly(G, vis):
    print(f'Preparing graph using {vis} layout (up to 5 min)')
    start = time()
    if vis == 'kamada_kawai':
        positions = nx.drawing.kamada_kawai_layout(G)
    elif vis == 'fruchterman_reingold':
        positions = nx.drawing.fruchterman_reingold_layout(G)
    print(f'Took {time() - start} seconds')
    edge_x = []
    edge_y = []
    print('Visualizing')
    for edge in G.edges():
        x0, y0 = positions[edge[0]]
        x1, y1 = positions[edge[1]]
        edge_x.append(x0)
        edge_x.append(x1)
        edge_x.append(None)
        edge_y.append(y0)
        edge_y.append(y1)
        edge_y.append(None)
    edge_trace = go.Scatter(
        x=edge_x, y=edge_y,
        line=dict(width=0.5, color='#888'),
        hoverinfo='none',
        mode='lines')
    node_x = []
    node_y = []
    for node in G.nodes():
        x, y = positions[node]
        node_x.append(x)
        node_y.append(y)

    node_trace = go.Scatter(
        x=node_x, y=node_y,
        mode='markers',
        hoverinfo='text',
        marker=dict(
            showscale=True,
            reversescale=True,
            color=[],
            size=10,
            colorbar=dict(
                thickness=15,
                title='Node Connections',
                xanchor='left',
                titleside='right'
            ),
            line_width=2))
    node_adjacencies = []
    node_text = []
    for node, adjacencies in enumerate(G.adjacency()):
        node_adjacencies.append(len(adjacencies[1]))
        node_text.append(
            f'Open friends of {adjacencies[0]}: {len(adjacencies[1])}')

    node_trace.marker.color = node_adjacencies
    node_trace.text = node_text
    fig = go.Figure(data=[edge_trace, node_trace],
                    layout=go.Layout(
        titlefont_size=16,
        showlegend=False,
        hovermode='closest',
        margin=dict(b=20, l=5, r=5, t=40),
        annotations=[dict(
            showarrow=False,
            xref="paper", yref="paper",
            x=0.005, y=-0.002)],
        xaxis=dict(showgrid=False, zeroline=False, showticklabels=False),
        yaxis=dict(showgrid=False, zeroline=False, showticklabels=False))
    )
    fig.show()

def main(file, layout):
    df = pd.read_csv(file)
    graph = nx.from_pandas_edgelist(df)
    display_with_plotly(graph, layout)

if __name__ == "__main__":
    args = parser.parse_args()
    main(args.file, args.layout)
